CC = g++
CCFLAGS = -g --std=c++14 -o $@
FLEX = flex
BISON = bison
all: latc

clean:
	rm -f *.o LatteCPP.dvi LatteCPP.aux LatteCPP.log latc_x86_64
	rm -f LatteCPP.pdf TestLatteCPP
	rm -f lib/*.o lib/*.gch
	rm -f src/*.o src/*.gch
	rm -f obj/*

latc: obj/latc_ src/latc.sh obj/lib.o
	#cp src/latc.sh ./latc
	#chmod +x ./latc
	echo "#!/bin/bash" > ./latc_x86_64
	echo . \"`pwd`/src/latc.sh\" \"\$$1\" \"`pwd`\" >> ./latc_x86_64
	chmod +x ./latc_x86_64

obj/lib.o: src/lib.c
	gcc -c $< -o $@

obj/latc_: lib/Absyn.o lib/Lexer.o lib/Parser.o src/identifier_map.o src/type_checker.o lib/Printer.o src/latc.cc src/err.o src/compiler.o
	${CC} ${CCFLAGS} lib/*.o src/*.o src/latc.cc

TestLatteCPP: lib/Absyn.o lib/Lexer.o lib/Parser.o lib/Printer.o lib/Test.o
	@echo "Linking TestLatteCPP..."
	${CC} ${CCFLAGS} lib/*.o

lib/Absyn.o: lib/Absyn.C lib/Absyn.H
	${CC} ${CCFLAGS} -c lib/Absyn.C

lib/Lexer.C: lib/LatteCPP.l
	${FLEX} -w -olib/Lexer.C lib/LatteCPP.l

lib/Parser.C: lib/LatteCPP.y
	${BISON} lib/LatteCPP.y -o lib/Parser.C

lib/Lexer.o: lib/Lexer.C lib/Parser.H
	${CC} ${CCFLAGS} -w -c lib/Lexer.C

lib/Parser.o: lib/Parser.C lib/Absyn.H
	${CC} ${CCFLAGS} -c lib/Parser.C

lib/Printer.o: lib/Printer.C lib/Printer.H lib/Absyn.H
	${CC} ${CCFLAGS} -c lib/Printer.C

lib/Skeleton.o: lib/Skeleton.C lib/Skeleton.H lib/Absyn.H
	${CC} ${CCFLAGS} -c lib/Skeleton.C

lib/Test.o: lib/Test.C lib/Parser.H lib/Printer.H lib/Absyn.H
	${CC} ${CCFLAGS} -c lib/Test.C

src/function.o: src/function.cc src/type.h
	${CC} ${CCFLAGS} -c $<

src/variable.o: src/variable.cc src/type.h
	${CC} ${CCFLAGS} -c $<

src/identifier_map.o: src/identifier_map.cc src/type.h src/variable.o src/function.o
	${CC} ${CCFLAGS} -c $<

src/type_checker.o: src/type_checker.cc src/identifier_map.o lib/Absyn.o
	${CC} ${CCFLAGS} -c $<

src/compiler.o: src/compiler.cc src/compiler.h src/nullstream.h
	${CC} ${CCFLAGS} -c $<

src/err.o: src/err.cc src/type.h
	${CC} ${CCFLAGS} -c $<
