#ifndef COMPILER_TYPE_MAP_H_
#define COMPILER_TYPE_MAP_H_

#include <unordered_map>
#include <tuple>
#include <string>
#include <vector>

#include "variable.h"
#include "function.h"

namespace compiler {

// Mapping from variable/function identifier to its representation.
class IdentifierMap {
 public:
  // Returns true iff given identifier is declared as a variable.
  bool IsVariable(const std::string& identifier) const;

  // Return true iff given identifier is declared as a function.
  bool IsFunction(const std::string& identifier) const;

  // Returns a Variable with given identifier.
  // If such Variable is not declared, then behavriour of this function
  // is not defined.
  Variable GetVariable(const std::string& identifier) const;
  
  // Returns a Function with given identifier.
  // If such Function is not declared, then behavriour of this function
  // is not defined.
  Function GetFunction(const std::string& identifier) const;

  // Declare a new Variable with name identifier.
  // If variable with such identifier was allready declared, it will be
  // overriden.
  void DeclareVariable(const std::string& identifier,
      const Variable& variable);

  // Declare a new Function with name identifier.
  // If function with such identifier was allready declared, it will be
  // overriden.
  void DeclareFunction(const std::string& identifier,
      const Function& function);

  // Forgets all declared variables.
  void ClearVariables();

  // Stores map of variables to reuse it later.
  std::unordered_map<std::string, Variable> GetVariables() const;

  // Restore variables.
  void RestoreVariables(const std::unordered_map<std::string, Variable>&
      variables);

 private:
  // Mapping from variable names to their types.
  std::unordered_map<std::string, Variable> variables_;
  
  // Mapping from function names to <vector of argument types, return type>.
  std::unordered_map<std::string, Function> functions_;
};

}  // namespace compiler

#endif
