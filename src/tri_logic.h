#ifndef TRI_LOGIC_H_
#define TRI_LOGIC_H_

namespace compiler {

// This enum represents 3 value boolean logic. Each statement can be evaluated
// to TRUE, FALSE or UNKNOWN. A statement is UNKNOWN if it can be either TRUE or
// FALSE and the corrent value can't be calculated in compile time.
// Example:
// int x = ReadInt();
// boolean b = x > 0;
//
// value of b is UNKNOWN.
enum class TriLogic {TRUE, FALSE, UNKNOWN};

}  // namespace compiler

#endif
