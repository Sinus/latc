#include "identifier_map.h"
#include "type.h"
#include "variable.h"

namespace compiler {

using std::string;
using std::vector;
using std::unordered_map;

bool IdentifierMap::IsVariable(const string& identifier) const {
  return variables_.find(identifier) != variables_.end();
}

bool IdentifierMap::IsFunction(const string& identifier) const {
  return functions_.find(identifier) != functions_.end();
}

Variable IdentifierMap::GetVariable(const string& identifier) const {
  return variables_.find(identifier)->second;
}

Function IdentifierMap::GetFunction(const string& identifier) const {
  return functions_.find(identifier)->second;
}

void IdentifierMap::DeclareVariable(const string& identifier,
    const Variable& variable) {
  variables_[identifier] = variable;
}

void IdentifierMap::DeclareFunction(const string& identifier,
    const Function& function) {
  functions_[identifier] = function;
}

void IdentifierMap::ClearVariables() {
  variables_.clear();
}

unordered_map<string, Variable> IdentifierMap::GetVariables() const {
  return variables_;
}

void IdentifierMap::RestoreVariables(
    const unordered_map<string, Variable>& variables) {
  variables_ = variables;
}

}  // namespace compiler
