#include "function.h"

using std::vector;

namespace compiler {

Function::Function(const vector<type>& argument_types,
    const type& return_type)
  : argument_types_(argument_types),
    return_type_(return_type) {}

type Function::GetReturnType() const {
  return return_type_;
}

vector<type> Function::GetArgumentTypes() const {
  return argument_types_;
}

}  // namespace compiler
