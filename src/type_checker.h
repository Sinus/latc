#ifndef TYPE_CHECKER_H_
#define TYPE_CHECKER_H_

#include <unordered_set>
#include <string>

#include "../lib/Absyn.H"
#include "identifier_map.h"
#include "tri_logic.h"

namespace compiler {

class TypeChecker : public Visitor
{
 public:
  void accept(Visitable* v);
  void visitProgram(Program* p);
  void visitTopDef(TopDef* p);
  void visitArg(Arg* p);
  void visitBlock(Block* p);
  void visitStmt(Stmt* p);
  void visitItem(Item* p);
  void visitType(Type* p);
  void visitExpr(Expr* p);
  void visitAddOp(AddOp* p);
  void visitMulOp(MulOp* p);
  void visitRelOp(RelOp* p);
  void visitProg(Prog* p);
  void visitFnDef(FnDef* p);
  void visitAr(Ar* p);
  void visitBlk(Blk* p);
  void visitEmpty(Empty* p);
  void visitBStmt(BStmt* p);
  void visitDecl(Decl* p);
  void visitNoInit(NoInit* p);
  void visitInit(Init* p);
  void visitAss(Ass* p);
  void visitIncr(Incr* p);
  void visitDecr(Decr* p);
  void visitRet(Ret* p);
  void visitVRet(VRet* p);
  void visitCond(Cond* p);
  void visitCondElse(CondElse* p);
  void visitWhile(While* p);
  void visitSExp(SExp* p);
  void visitInt(Int* p);
  void visitStr(Str* p);
  void visitBool(Bool* p);
  void visitVoid(Void* p);
  void visitFun(Fun* p);
  void visitEVar(EVar* p);
  void visitELitInt(ELitInt* p);
  void visitELitTrue(ELitTrue* p);
  void visitELitFalse(ELitFalse* p);
  void visitEApp(EApp* p);
  void visitEString(EString* p);
  void visitNeg(Neg* p);
  void visitNot(Not* p);
  void visitEMul(EMul* p);
  void visitEAdd(EAdd* p);
  void visitERel(ERel* p);
  void visitEAnd(EAnd* p);
  void visitEOr(EOr* p);
  void visitPlus(Plus* p);
  void visitMinus(Minus* p);
  void visitTimes(Times* p);
  void visitDiv(Div* p);
  void visitMod(Mod* p);
  void visitLTH(LTH* p);
  void visitLE(LE* p);
  void visitGTH(GTH* p);
  void visitGE(GE* p);
  void visitEQU(EQU* p);
  void visitNE(NE* p);
  void visitListTopDef(ListTopDef* p);
  void visitListArg(ListArg* p);
  void visitListStmt(ListStmt* p);
  void visitListItem(ListItem* p);
  void visitListType(ListType* p);
  void visitListExpr(ListExpr* p);

  void visitInteger(Integer x);
  void visitChar(Char x);
  void visitDouble(Double x);
  void visitString(String x);
  void visitIdent(Ident x);

  IdentifierMap* GetIdentifierMap();

  TriLogic EvalsTo(Expr* expr) const;

 private:
  // Map of used identifiers.
  IdentifierMap identifier_map_;

  // Set of identifiers declared inside given block.
  std::unordered_set<std::string> identifiers_block_;

  // Copy of identifier_map_, used by Store/Restore Environment.
  std::unordered_map<std::string, Variable> identifier_map_copy;

  // Set to true when visiting a return and to false before entering the body.
  bool returned_;

  // Copy of identifiers_block_, used by Store/Restore Environment.
  std::unordered_set<std::string> identifiers_block_copy_;

  // Type which function, which is currently visited, should return.
  type expected_return_type_;
  
  // Given an expression returns its type or raises error if it occurs.
  type ExpressionType(Expr* expr) const;

  // Recursively checks types for biargumental expressions (EOr, EAdd etc).
  template<class T>
  void RecursiveExpressionCheck(T* expr, type expected) const;

  // Returns type of Expr1 == Expr2 expression and raises error if they don't
  // match.
  type CheckEqual(ERel* expr) const;

  // Returns type of Expr1 + Expr2 and raises error if types don't match or are
  // not int or string.
  type CheckAdd(EAdd* expr) const;

  // Check if expression in T cond is of boolean type.
  // Raise error if not.
  template<class T>
  void CheckBool(T cond) const;

  // Stores current identifier_map_ and identifiers_block_.
  // Those values can be later restored using RestoreEnvironment().
  // Cleares identifiers_block_.
  void StoreEnvironment();

  // Sets identifier_map_ and identifiers_block_ to values stored by
  // StoreEnvironment().
  // NOTE: If RestoreEnvironment() is used when no Environment is stored, the
  // behaviour is not defined.
  void RestoreEnvironment();

  // Declare a function.
  // This is outside of visitFnDef to declare all functions before they are
  // used.
  void DeclareFunction(FnDef *fndef);
};

}  // namespace compiler

#endif
