#!/bin/bash

set -e

if [ $# -ne 2 ]
then
  echo "Usage: $0 source.lat"
  exit
fi

file_name=${1::-4}
"$2"/obj/latc_ "$file_name".lat
if [ $? -ne 0 ]
then
  rm "$file_name".s -f
  exit
fi
echo "OK"
nasm -f elf64 -o "$file_name".o "$file_name".s
gcc "$file_name".o "$2"/obj/lib.o -o "$file_name"
rm -f "$file_name".o
