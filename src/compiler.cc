/*** BNFC-Generated Visitor Design Pattern Compiler. ***/
/* This implements the common visitor design pattern.
   Note that this method uses Visitor-traversal of lists, so
   List->accept() does NOT traverse the list. This allows different
   algorithms to use context information differently. */

#include <iostream>
#include <ostream>
#include <string>
#include <regex>

#include "compiler.h"
#include "nullstream.h"

using std::string;
using std::ostream;
using std::endl;

namespace compiler {
namespace {

constexpr char kGlobalStart[] = "global _start";
constexpr char kGlobalMain[] = "global main";
constexpr char kExternPrintf[] = "extern printf";
constexpr char kExternScanf[] = "extern scanf";
constexpr char kExternMalloc[] = "extern malloc";
constexpr char kExternStrlen[] = "extern strlen";
constexpr char kExternStrcpy[] = "extern strcpy";
constexpr char kExternReadInt[] = "extern readInt";
constexpr char kExternGetline[] = "extern getline";
constexpr char kExternReadString[] = "extern readString";
constexpr char kExternAddReference[] = "extern add_reference";
constexpr char kExternGcInit[] = "extern gc_init";
constexpr char kExternUnlink[] = "extern unlink";
constexpr char kExternAddStrings[] = "extern add_strings";
constexpr char kExternPrintInt[] = "extern printInt";
constexpr char kExternPrintString[] = "extern printString";
constexpr char kStartLabel[] = "_start:";
constexpr char kGotoMain[] = "jmp main";
const string kLabelPrefix = "_L";
const string kLabel = ":";
const string kSysRead = "0";
const string kStringBuffer = "128";
constexpr char kSectionText[] = "section .text";
constexpr char kSectionData[] = "section .data";
constexpr char kRuntimeError[] = "runtime error";
const string kFsb = "fsb";
const string kFsbRead = "fsb_read";
const string kFsbString = "fsb_string";
const string kStringFormatString = "\"\%s\",0x00";
const string kStringFormatInt = "\"\%lld\",0xa,0x00";
const string kStringFormatRead = "\"\%lld\",0x00";
const string kStringConstantPrefix = "s";
constexpr char kStringSize[] = " db ";
constexpr char kIntSize[] = " dq ";
constexpr char kPrintInt[] = "printInt";
constexpr char kReadInt[] = "readInt";
constexpr char kPrintString[] = "printString";
constexpr char kReadString[] = "readString";
constexpr char kError[] = "error";
constexpr char kSyscallNumber[] = "rax";
constexpr char kVariadicNumberOfArgsReg[] = "rax";
const string kCall = "call ";
const string kPrintf = "printf";
constexpr char kExpressionValueReg[] = "rax";
const string kSecondaryArithmeticReg = "rcx";
const string kRemainderReg = "rdx";
constexpr char kArg1Register[] = "rdi";
constexpr char kArg2Register[] = "rsi";
constexpr char kArg3Register[] = "rdx";
constexpr char kArg4Register[] = "rcx";
constexpr char kArg5Register[] = "r8";
constexpr char kArg6Register[] = "r9";
constexpr char kStackPointer[] = "rsp";
const string kBasePointer[] = "rbp";
constexpr char kSysWrite[] = "1";
constexpr char kSyscall[] = "syscall";
constexpr char kSysExit[] = "60";
constexpr char kProlog[] = "enter 0, 0";
constexpr char kEpilog[] = "leave";
const string kR8 = "r8";
const string kR9 = "r9";
const string kR10 = "r10";
constexpr char kRet[] = "ret";
const string kTrue = "1";
const string kFalse = "0";
const string kTempInt = "temp_int";

}  // namespace

void Compiler::visitProgram(Program* t) {} //abstract class
void Compiler::visitTopDef(TopDef* t) {} //abstract class
void Compiler::visitArg(Arg* t) {} //abstract class
void Compiler::visitBlock(Block* t) {} //abstract class
void Compiler::visitStmt(Stmt* t) {} //abstract class
void Compiler::visitItem(Item* t) {} //abstract class
void Compiler::visitType(Type* t) {} //abstract class
void Compiler::visitExpr(Expr* t) {} //abstract class
void Compiler::visitAddOp(AddOp* t) {} //abstract class
void Compiler::visitMulOp(MulOp* t) {} //abstract class
void Compiler::visitRelOp(RelOp* t) {} //abstract class

void Compiler::CalculateExpression(Expr* expr, bool is_string) {
  using std::to_string;
  if (ELitInt* elit = dynamic_cast<ELitInt*>(expr)) {
    Bytecode_() << Mov_(kExpressionValueReg, to_string(elit->integer_));
  } else if (EVar* evar = dynamic_cast<EVar*>(expr)) {
    // TODO(mdominiak): Check size of variable and move accordingly.
    Bytecode_() << Mov_(kExpressionValueReg, 
                        "[" + identifier_map_->GetVariable(evar->ident_)
                                .GetStackLocation() + "]");
  } else if (EAdd* eadd = dynamic_cast<EAdd*>(expr)) {
    CalculateExpression(eadd->expr_1, is_string);
    Bytecode_() << "push " << kExpressionValueReg << endl;
    CalculateExpression(eadd->expr_2);
    Bytecode_() << Mov_(kSecondaryArithmeticReg, kExpressionValueReg);
    Bytecode_() << "pop " << kExpressionValueReg << endl;
    if (Plus* _ = dynamic_cast<Plus*>(eadd->addop_)) {
      if (is_string) {
        Bytecode_() << "mov rdi, rax" << endl
                    << "mov rsi, rcx" << endl
                    << "call add_strings" << endl;
      } else {
        Bytecode_() << "add " << kExpressionValueReg << ", " 
                    << kSecondaryArithmeticReg << endl;
      }
     } else if (Minus* _ = dynamic_cast<Minus*>(eadd->addop_)) {
      Bytecode_() << "sub " << kExpressionValueReg << ", "
                  << kSecondaryArithmeticReg << endl;
     }
  } else if (EMul* emul = dynamic_cast<EMul*>(expr)) {
    Bytecode_() << Mov_("rdx", "0");
    CalculateExpression(emul->expr_1);
    Bytecode_() << "push " << kExpressionValueReg << endl;
    CalculateExpression(emul->expr_2);
    Bytecode_() << Mov_(kSecondaryArithmeticReg, kExpressionValueReg);
    Bytecode_() << "pop " << kExpressionValueReg << endl;
    // TODO(mdominiak): Check for overflows?
    if (Times* _ = dynamic_cast<Times*>(emul->mulop_))
      Bytecode_() << "imul " << kExpressionValueReg << ", "
                  << kSecondaryArithmeticReg << endl;
    else if (Div* _ = dynamic_cast<Div*>(emul->mulop_))
      Bytecode_() << "idiv "
                  << kSecondaryArithmeticReg << endl;
    else if (Mod* _ = dynamic_cast<Mod*>(emul->mulop_))
      // When A is in RAX and B in RCX, then
      // mov rdx, 0
      // idiv rcx
      // will store remainder in rdx.
      Bytecode_() << "idiv " << kSecondaryArithmeticReg << endl
                  << Mov_(kExpressionValueReg, kRemainderReg);
  } else if (Neg* neg = dynamic_cast<Neg*>(expr)) {
    CalculateExpression(neg->expr_);
    Bytecode_() << "imul " << kExpressionValueReg << ", -1" << endl;
  } else if (ELitTrue* etrue = dynamic_cast<ELitTrue*>(expr)) {
    Bytecode_() << Mov_(kExpressionValueReg, kTrue);
  } else if (ELitFalse* efalse = dynamic_cast<ELitFalse*>(expr)) {
    Bytecode_() << Mov_(kExpressionValueReg, kFalse);
  } else if (Not* enot = dynamic_cast<Not*>(expr)) {
    CalculateExpression(enot->expr_);
    Bytecode_() << "xor " << kExpressionValueReg << ", " << kTrue << endl;
  } else if (EOr* eor = dynamic_cast<EOr*>(expr)) {
    auto label = GetLabel();
    CalculateExpression(eor->expr_1);
    Bytecode_() << "cmp " << kExpressionValueReg << ", " << kTrue << endl
                << "je " << kLabelPrefix << label << endl;
    // Since we reach this code, expr_1 was false.
    // So the result of this or expr is value of expr_2.
    CalculateExpression(eor->expr_2);
    Bytecode_() << kLabelPrefix << label << kLabel << endl;
  } else if (EAnd* eand = dynamic_cast<EAnd*>(expr)) {
    auto label = GetLabel();
    CalculateExpression(eand->expr_1);
    Bytecode_() << "cmp " << kExpressionValueReg << ", " << kFalse << endl
                << "je " << kLabelPrefix << label << endl;
    // Since we reach this code, expr_1 was true.
    // So the result of this and expr is value of expr_2.
    CalculateExpression(eand->expr_2);
    Bytecode_() << kLabelPrefix << label << kLabel << endl;
  } else if (ERel* erel = dynamic_cast<ERel*>(expr)) {
    string reg_true = kR9, reg_false = kR10;
    string conditional_mov_suffix;
    CalculateExpression(erel->expr_1);
    Bytecode_() << Mov_(reg_true, kTrue) << Mov_(reg_false, kFalse)
                << "push " << kExpressionValueReg << endl;
    CalculateExpression(erel->expr_2);
    Bytecode_() << "pop " << kSecondaryArithmeticReg << endl;
    if (LTH* _ = dynamic_cast<LTH*>(erel->relop_))
      conditional_mov_suffix = "l";
    else if (LE* _ = dynamic_cast<LE*>(erel->relop_))
      conditional_mov_suffix = "le";
    else if (GTH* _ = dynamic_cast<GTH*>(erel->relop_))
      conditional_mov_suffix = "g";
    else if (GE* _ = dynamic_cast<GE*>(erel->relop_))
      conditional_mov_suffix = "ge";
    else if (EQU* _ = dynamic_cast<EQU*>(erel->relop_))
      conditional_mov_suffix = "e";
    else if (NE* _ = dynamic_cast<NE*>(erel->relop_))
      conditional_mov_suffix = "ne";
    
    Bytecode_() << Mov_(kR10, "0")
                << "cmp " << kSecondaryArithmeticReg << ", " << kExpressionValueReg << endl
                << "cmov" << conditional_mov_suffix << " " << kR10 << ", " << reg_true << endl
                << "cmp " << kR10 << ", " << kTrue << endl
                << "cmove " << kExpressionValueReg << ", " << reg_true << endl
                << "cmovne " << kExpressionValueReg << ", " << reg_false << endl; 
  } else if (EApp* eapp = dynamic_cast<EApp*>(expr)) {
    visitEApp(eapp);
  } else if (EString* estr = dynamic_cast<EString*>(expr)) {
    constant_strings_.push_back(estr->string_);
    Bytecode_() << Lea_(kExpressionValueReg, kStringConstantPrefix +
                        to_string(constant_strings_.size() - 1));
  }
  else {
    // We don't know what this expression is, this should never happen.
    Bytecode_() << "mov rax, 0" << endl;
  }
}

void Compiler::accept(Visitable* v) {
  v->accept(this);
}

void Compiler::SetOuputStream(ostream& ostream) {
  stream_ = &ostream;
}

ostream& Compiler::Bytecode_() const {
  return *stream_;
}

void Compiler::SetIdentifiersMap(IdentifierMap* identifier_map) {
  identifier_map_ = identifier_map;
  identifier_map_->ClearVariables();
}

void Compiler::visitProg(Prog *prog) {
  using std::to_string;
  Bytecode_() << kGlobalMain << endl
              << kExternPrintf << endl
              << kExternScanf << endl
              << kExternMalloc << endl
              << kExternStrlen << endl
              << kExternAddStrings<< endl
              << kExternStrcpy << endl
              << kExternAddReference << endl
              << kExternUnlink << endl
              << kExternGcInit << endl
              << kExternReadString << endl
              << kExternReadInt << endl
              << kExternPrintInt << endl
              << kExternPrintString << endl
              << kSectionText << endl << endl
              << kStartLabel << endl
              << kGotoMain << endl << endl;
  constant_strings_.push_back(kRuntimeError);
  
  prog->listtopdef_->accept(this);
  
  // Save all constants to bytecode;
  Bytecode_() << endl << endl;  // Just for bytecode readbility.

  Bytecode_() << kSectionData << endl;

  Bytecode_() << kFsb << kStringSize << kStringFormatInt << endl
              << kFsbRead << kStringSize << kStringFormatRead << endl
              << kFsbString << kStringSize << kStringFormatString << endl
              << kTempInt << kIntSize << 0 << endl;

  for (int i = 0; i < constant_strings_.size(); ++i) {
    string safe;
    for (char c : constant_strings_[i]) {
      if (c != '`')
        safe += c;
      else
        safe += "\\`";
    }
    Bytecode_() << kStringConstantPrefix << to_string(i) << kStringSize 
      << "`" << safe << "`,0x00" << endl;
  }
}

void Compiler::visitFnDef(FnDef *fndef) {
  using std::to_string;

  int old_rbp_offset = current_rbp_offset_;
  current_rbp_offset_ = 0;

  Bytecode_() << fndef->ident_ << kLabel << endl
    << kProlog << endl;
  if (fndef->ident_ == "main")
    Bytecode_() << "call gc_init" << endl;  // Initializes garbage collector.
  
  StoreEnvironment();
  identifier_map_->ClearVariables();

  auto arg_rbp_offset = 24;
  for (int i = fndef->listarg_->size() - 1; i >= 0; --i) {
    if (Ar* ar = dynamic_cast<Ar*>(fndef->listarg_->at(i))) {
      
      int bytesize = 8;
      Bytecode_() << Sub_(kStackPointer, to_string(bytesize));
      Variable var(types::TypeClassToType(ar->type_));
      var.SetStackLocation("rbp - " + to_string(current_rbp_offset_));
      current_rbp_offset_ += bytesize;
      identifier_map_->DeclareVariable(ar->ident_, var);


      // Move value of this variable to RAX.
      // This is OK, because argument is never passed in rax.
      if (i == 0) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg1Register);
      } else if (i == 1) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg2Register);
      } else if (i == 2) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg3Register);
      } else if (i == 3) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg4Register);
      } else if (i == 4) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg5Register);
      } else if (i == 5) {
        Bytecode_() << Mov_(kExpressionValueReg, kArg6Register);
      } else {
        // First arugment on stack is at rbp + 16, because at rbp
        // there is saved rbp, then at rbp + 8 return address and then
        // arguments.
        Bytecode_() << Mov_(kExpressionValueReg,
                            "[rbp + " + to_string(arg_rbp_offset) + "]");
        arg_rbp_offset += 8;
      }
      // Assign correct value to argument.
      Assign(identifier_map_->GetVariable(ar->ident_));
    }
  }

  fndef->type_->accept(this);
  visitIdent(fndef->ident_);
  fndef->listarg_->accept(this);
  current_function_returns_string_ = (types::TypeClassToType(fndef->type_) == types::type_string);
  fndef->block_->accept(this);

  Bytecode_() << kEpilog << endl << kRet << endl << endl;
  RestoreEnvironment();
  current_rbp_offset_ = old_rbp_offset;
}

void Compiler::visitAr(Ar *ar)
{
  /* Code For Ar Goes Here */

  ar->type_->accept(this);
  visitIdent(ar->ident_);

}

void Compiler::visitBlk(Blk *blk)
{
  /* Code For Blk Goes Here */

  blk->liststmt_->accept(this);

}

void Compiler::visitEmpty(Empty *empty)
{
  /* Code For Empty Goes Here */


}

void Compiler::Assign(const Variable& variable) const {
  Bytecode_() << "mov [" << variable.GetStackLocation() << "], " 
              << kExpressionValueReg << endl;
}

void Compiler::visitBStmt(BStmt *bstmt) {
  auto env = identifier_map_->GetVariables();
  bstmt->block_->accept(this);
  identifier_map_->RestoreVariables(env);
}

void Compiler::visitDecl(Decl *decl) {
  using std::to_string;

  int bytesize = 8;
  auto type = types::TypeClassToType(decl->type_);

  if (!in_while_)
    Bytecode_() << Sub_(kStackPointer,
                        to_string(bytesize * decl->listitem_->size()));
  else
    rsp_modif_ += bytesize * decl->listitem_->size();
  
  for (int i = 0; i < decl->listitem_->size(); ++i) {
    Variable var(type);
    var.SetStackLocation("rbp - " + to_string(current_rbp_offset_));
    current_rbp_offset_ += bytesize;
    if (NoInit* noinit = dynamic_cast<NoInit*>(decl->listitem_->at(i))) {
      identifier_map_->DeclareVariable(noinit->ident_, var);
      if (type == types::type_int || type == types::type_bool) {
        Bytecode_() << Mov_(kExpressionValueReg, "0");
        Assign(var);
      }
    } else if (Init* init = dynamic_cast<Init*>(decl->listitem_->at(i))) {
      bool is_string = false;
      if (type == types::type_string)
        is_string = true;
      CalculateExpression(init->expr_, is_string);
      identifier_map_->DeclareVariable(init->ident_, var);
      Assign(var);
      if (is_string) {
        Bytecode_() << Mov_(kArg1Register, "[" + var.GetStackLocation() + "]")
                    << "call add_reference" << endl;
      }
    }
  }
}

void Compiler::visitAss(Ass *ass) {
  bool is_string = false;
  if (identifier_map_->GetVariable(ass->ident_).GetType() == types::type_string) {
    is_string = true;
    auto lhs_addr = identifier_map_->GetVariable(ass->ident_)
                                        .GetStackLocation();

    // Store current location of lhs in r9, it might be usefull for GC later.
    Bytecode_() << Mov_("r9", "[" + lhs_addr + "]")
                << "push r9" << endl;
  }
  CalculateExpression(ass->expr_, is_string);
  Assign(identifier_map_->GetVariable(ass->ident_));

  if (is_string) {
    // Manage garbage collection.
    if (EVar* evar = dynamic_cast<EVar*>(ass->expr_)) {
      if (evar->ident_ == ass->ident_) {
        // This is assignemnt of type
        // string x = "abc";
        // x = x;
        // No need to do any GC here.
      } else {
        // This is assignment of type
        // string x = "a", string y = "b";
        // x = y;
        // Unlink x and add another reference to y.

        auto rhs_addr = "[" + identifier_map_->GetVariable(evar->ident_)
          .GetStackLocation() + "]";
        Bytecode_() << "pop r9" << endl
                    << Mov_(kArg1Register, "r9")
                    << "call unlink" << endl
                    << Mov_(kArg1Register, "[" + rhs_addr + "]")
                    << "call add_reference" << endl;
      }
    } else {
      // This is assignment of type
      // string x;
      // x = A + B
      // or 
      // x = "abc";
      // In both cases unlink previous value of x and store
      // reference to the new one.

      auto new_addr = identifier_map_->GetVariable(ass->ident_)
                                        .GetStackLocation();
      Bytecode_() << "pop r9" << endl
                  << Mov_(kArg1Register, "r9")
                  << "call unlink" << endl
                  << Mov_(kArg1Register, "[" + new_addr + "]")
                  << "call add_reference" << endl;
    }
  }
}

void Compiler::visitIncr(Incr *incr) {
  Bytecode_() << Mov_(kExpressionValueReg,
                      "[" + identifier_map_->GetVariable(incr->ident_)
                        .GetStackLocation()
                      + "]");
  Bytecode_() << "add " << kExpressionValueReg << ", 1" << endl;
  Assign(identifier_map_->GetVariable(incr->ident_));
}

void Compiler::visitDecr(Decr *decr) {
  Bytecode_() << Mov_(kExpressionValueReg,
                      "[" + identifier_map_->GetVariable(decr->ident_)
                        .GetStackLocation()
                      + "]");
  Bytecode_() << "sub " << kExpressionValueReg << ", 1" << endl;
  Assign(identifier_map_->GetVariable(decr->ident_));
}

void Compiler::visitRet(Ret *ret) { 
  CalculateExpression(ret->expr_, current_function_returns_string_);
  // Return value is in rax, so correct.
  Bytecode_() << "leave" << endl << "ret" << endl;
}

void Compiler::visitVRet(VRet *vret) {
  Bytecode_() << "leave" << endl << "ret" << endl;
}

int Compiler::GetLabel() {
  return free_label_++;
}

void Compiler::visitCond(Cond *cond) {
  /* Code For Cond Goes Here */

  //cond->expr_->accept(this);
  //cond->stmt_->accept(this);

  int label = GetLabel();
  
  auto evals_to = type_checker_.EvalsTo(cond->expr_);
  if (evals_to == TriLogic::UNKNOWN) {
    CalculateExpression(cond->expr_);
    Bytecode_() << "cmp " << kExpressionValueReg << ", " << kFalse << endl
      << "je " << kLabelPrefix << label << endl;
    cond->stmt_->accept(this);
    Bytecode_() << kLabelPrefix << label << kLabel << endl;
  } else if (evals_to == TriLogic::TRUE) {
    cond->stmt_->accept(this);
  } else {
    // Condition is always false, so no need to calculate anything.
  }
}

void Compiler::visitCondElse(CondElse *condelse)
{
  /* Code For CondElse Goes Here */

  int label_true = GetLabel(), label_end = GetLabel();
  CalculateExpression(condelse->expr_);
  auto evals_to = type_checker_.EvalsTo(condelse->expr_);
  if (evals_to == TriLogic::UNKNOWN) {
    Bytecode_() << "cmp " << kExpressionValueReg << ", " << kTrue << endl
      << "je " << kLabelPrefix << label_true << endl;
    condelse->stmt_2->accept(this);  // The condition is false.
    Bytecode_() << "jmp " << kLabelPrefix << label_end << endl
      << kLabelPrefix << label_true << kLabel << endl;
    condelse->stmt_1->accept(this);
    Bytecode_() << kLabelPrefix << label_end << kLabel << endl;
  } else if (evals_to == TriLogic::TRUE) {
    // Else will never be visited.
    condelse->stmt_1->accept(this);
  } else {
    // Else will always be visited.
    condelse->stmt_2->accept(this);
  }
}

void Compiler::visitWhile(While *swhile) {
  using std::to_string;
  if (type_checker_.EvalsTo(swhile->expr_) == TriLogic::FALSE) {
    // The body will never be visited.
    return;
  }

  // Dry-run a while statement to calculate how to modify rsp
  // before actually printing out the code.
  // To do that swap output stream with a "black hole" stream.
  auto actual_stream = stream_;
  NullStream null_stream;
  stream_ = &null_stream;

  auto copy_in_while = in_while_;
  in_while_ = true;
  auto current_rbp = current_rbp_offset_;
  swhile->stmt_->accept(this);
  current_rbp_offset_ = current_rbp;
  in_while_ = copy_in_while;

  stream_ = actual_stream;  // Swap output stream back to real one.

  if (in_while_ == false) {
    Bytecode_() << "sub rsp, " << to_string(rsp_modif_) << endl;
    rsp_modif_ = 0;
  }

  int label_cond = GetLabel(), label_body = GetLabel();
  Bytecode_() << "jmp " << kLabelPrefix << label_cond << endl
              << kLabelPrefix << label_body << kLabel << endl;

  auto current_in_while = in_while_;
  in_while_ = true;
  swhile->stmt_->accept(this);
  in_while_ = current_in_while;

  Bytecode_() << kLabelPrefix << label_cond << kLabel << endl;
  CalculateExpression(swhile->expr_);
  Bytecode_() << "cmp " << kExpressionValueReg << ", " << kTrue << endl
              << "je " << kLabelPrefix << label_body << endl;
}

void Compiler::visitSExp(SExp *sexp) {
  // Intruction-expressions don't have to be compiled in any way.
  if (EApp* eapp = dynamic_cast<EApp*>(sexp->expr_))
    sexp->expr_->accept(this);
}

void Compiler::visitNoInit(NoInit *noinit)
{
  /* Code For NoInit Goes Here */
  
  visitIdent(noinit->ident_);
}

void Compiler::visitInit(Init *init)
{
  /* Code For Init Goes Here */

  visitIdent(init->ident_);
  init->expr_->accept(this);

}

void Compiler::visitInt(Int *eint)
{
  /* Code For Int Goes Here */


}

void Compiler::visitStr(Str *str)
{
  // Add this string constant to set of string constants.

}

void Compiler::visitBool(Bool *ebool)
{
  /* Code For Bool Goes Here */


}

void Compiler::visitVoid(Void *evoid)
{
  /* Code For Void Goes Here */


}

void Compiler::visitFun(Fun *fun)
{
  /* Code For Fun Goes Here */

  fun->type_->accept(this);
  fun->listtype_->accept(this);

}

void Compiler::visitEVar(EVar *evar)
{
  /* Code For EVar Goes Here */

  visitIdent(evar->ident_);

}

void Compiler::visitELitInt(ELitInt *elitint)
{
  /* Code For ELitInt Goes Here */

  visitInteger(elitint->integer_);

}

void Compiler::visitELitTrue(ELitTrue *elittrue)
{
  /* Code For ELitTrue Goes Here */


}

void Compiler::visitELitFalse(ELitFalse *elitfalse) {
  /* Code For ELitFalse Goes Here */


}

string Compiler::Mov_(string reg, string item) const {
  return "mov " + reg + ", " + item + "\n";
}

string Compiler::Lea_(string reg, string item) const {
  return "lea " + reg + ", [" + item + "]\n";
}

string Compiler::Sub_(string reg, string item) const {
  return "sub " + reg + ", " + item + "\n";
}

string Compiler::StringConstantLocation_(int location_number) const {
  using std::to_string;
  return "[" + kStringConstantPrefix + to_string(location_number) + "]";
}

string Compiler::StringConstantLength_(int location_number) const {
  using std::to_string;
  return to_string(constant_strings_[location_number].length());
}

string Compiler::WriteConstantToFd_(int constant, int fd) const {
  using std::to_string;
  return Mov_(kSyscallNumber, kSysWrite) +
    Mov_(kArg1Register, to_string(fd)) + 
    Lea_(kArg2Register, kStringConstantPrefix + to_string(constant)) +
    Mov_(kArg3Register, StringConstantLength_(constant)) + 
    kSyscall;
}

string Compiler::SysExit_(int return_code) const {
  using std::to_string;
  return Mov_(kSyscallNumber, kSysExit) +
    Mov_(kArg1Register,to_string(return_code)) +
    kSyscall;
}

void Compiler::StoreEnvironment() {
  identifier_map_copy_ = identifier_map_->GetVariables();
}

void Compiler::RestoreEnvironment() {
  identifier_map_->RestoreVariables(identifier_map_copy_);
}

void Compiler::visitEApp(EApp *eapp) {
  // TODO(mdominiak): Should any registers be saved? RAX/RCX?
  if (eapp->ident_ == kError) {
    Bytecode_() << WriteConstantToFd_(0, 2) << endl  // 2 for stderr.
      << SysExit_(-1) << endl;
  } else if (eapp->ident_ == kPrintInt) {
    CalculateExpression(eapp->listexpr_->at(0));
    Bytecode_() << Mov_(kArg1Register, kExpressionValueReg)
                << kCall << kPrintInt << endl;
  } else if (eapp->ident_ == kReadInt) {
    Bytecode_() << "call readInt" << endl;
  } else if (eapp->ident_ == kPrintString) {
    CalculateExpression(eapp->listexpr_->at(0), true);
    Bytecode_() << Mov_(kArg1Register, kExpressionValueReg)
                << kCall << kPrintString << endl;
  } else if (eapp->ident_ == kReadString) {
    Bytecode_() << "call readString" << endl;
  } else {
    auto arg_types = identifier_map_->GetFunction(eapp->ident_)
                                        .GetArgumentTypes();
    // Push arguments accordingly to System V ABI.
    for (int i = 0; i < eapp->listexpr_->size(); ++i) {
      bool is_string = false;
      if (arg_types[i] == types::type_string)
        is_string = true;
      CalculateExpression(eapp->listexpr_->at(i), is_string);
      if (i == 0)
        Bytecode_() << Mov_(kArg1Register, kExpressionValueReg);
      else if (i == 1)
        Bytecode_() << Mov_(kArg2Register, kExpressionValueReg);
      else if (i == 2)
        Bytecode_() << Mov_(kArg3Register, kExpressionValueReg);
      else if (i == 3)
        Bytecode_() << Mov_(kArg4Register, kExpressionValueReg);
      else if (i == 4)
        Bytecode_() << Mov_(kArg5Register, kExpressionValueReg);
      else if (i == 5)
        Bytecode_() << Mov_(kArg6Register, kExpressionValueReg);
      else
        Bytecode_() << "push " << kExpressionValueReg << endl;
    }
    Bytecode_() << "push rbp" << endl
                << "call " << eapp->ident_ << endl
                << "pop rbp" << endl;
  }
}

void Compiler::visitEString(EString *estring)
{
  /* Code For EString Goes Here */

  visitString(estring->string_);

}

void Compiler::visitNeg(Neg *neg)
{
  /* Code For Neg Goes Here */

  neg->expr_->accept(this);

}

void Compiler::visitNot(Not *enot)
{
  /* Code For Not Goes Here */

  enot->expr_->accept(this);

}

void Compiler::visitEMul(EMul *emul)
{
  /* Code For EMul Goes Here */

  emul->expr_1->accept(this);
  emul->mulop_->accept(this);
  emul->expr_2->accept(this);

}

void Compiler::visitEAdd(EAdd *eadd)
{
  /* Code For EAdd Goes Here */

  eadd->expr_1->accept(this);
  eadd->addop_->accept(this);
  eadd->expr_2->accept(this);

}

void Compiler::visitERel(ERel *erel)
{
  /* Code For ERel Goes Here */

  erel->expr_1->accept(this);
  erel->relop_->accept(this);
  erel->expr_2->accept(this);

}

void Compiler::visitEAnd(EAnd *eand)
{
  /* Code For EAnd Goes Here */

  eand->expr_1->accept(this);
  eand->expr_2->accept(this);

}

void Compiler::visitEOr(EOr *eor)
{
  /* Code For EOr Goes Here */

  eor->expr_1->accept(this);
  eor->expr_2->accept(this);

}

void Compiler::visitPlus(Plus *plus)
{
  /* Code For Plus Goes Here */


}

void Compiler::visitMinus(Minus *minus)
{
  /* Code For Minus Goes Here */


}

void Compiler::visitTimes(Times *times)
{
  /* Code For Times Goes Here */


}

void Compiler::visitDiv(Div *div)
{
  /* Code For Div Goes Here */


}

void Compiler::visitMod(Mod *mod)
{
  /* Code For Mod Goes Here */


}

void Compiler::visitLTH(LTH *lth)
{
  /* Code For LTH Goes Here */


}

void Compiler::visitLE(LE *le)
{
  /* Code For LE Goes Here */


}

void Compiler::visitGTH(GTH *gth)
{
  /* Code For GTH Goes Here */


}

void Compiler::visitGE(GE *ge)
{
  /* Code For GE Goes Here */


}

void Compiler::visitEQU(EQU *equ)
{
  /* Code For EQU Goes Here */


}

void Compiler::visitNE(NE *ne)
{
  /* Code For NE Goes Here */


}


void Compiler::visitListTopDef(ListTopDef* listtopdef)
{
  for (ListTopDef::iterator i = listtopdef->begin() ; i != listtopdef->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void Compiler::visitListArg(ListArg* listarg)
{
  for (ListArg::iterator i = listarg->begin() ; i != listarg->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void Compiler::visitListStmt(ListStmt* liststmt)
{
  for (ListStmt::iterator i = liststmt->begin() ; i != liststmt->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void Compiler::visitListItem(ListItem* listitem)
{
  for (ListItem::iterator i = listitem->begin() ; i != listitem->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void Compiler::visitListType(ListType* listtype)
{
  for (ListType::iterator i = listtype->begin() ; i != listtype->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void Compiler::visitListExpr(ListExpr* listexpr)
{
  for (ListExpr::iterator i = listexpr->begin() ; i != listexpr->end() ; ++i)
  {
    (*i)->accept(this);
  }
}


void Compiler::visitInteger(Integer x)
{
  /* Code for Integer Goes Here */
}

void Compiler::visitChar(Char x)
{
  /* Code for Char Goes Here */
}

void Compiler::visitDouble(Double x)
{
  /* Code for Double Goes Here */
}

void Compiler::visitString(String x)
{
  /* Code for String Goes Here */
}

void Compiler::visitIdent(Ident x)
{
  /* Code for Ident Goes Here */
}


}  // namespace compiler
