#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>

#include "../lib/Parser.H"
#include "../lib/Absyn.H"
#include "../lib/Printer.H"
#include "type_checker.h"
#include "compiler.h"

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using compiler::TypeChecker;
using compiler::Compiler;

const string kAsm = "s";

int main(int argc, char ** argv) {
  FILE *input;
  if (argc > 1)  {
    //cout << "argv[1]: " << argv[1] << endl;
    input = fopen(argv[1], "r");
    if (!input) {
      fprintf(stderr, "Error opening input file.\n");
      exit(1);
    }
  }
  else input = stdin;

  if (argc != 2) {
    cerr << "Usage: " << argv[0] << " input.lat" << endl;
    exit(EXIT_FAILURE);
  }



  /* The default entry point is used. For other options see Parser.H */
  Program *parse_tree = pProgram(input);
  if (parse_tree) {
    TypeChecker type_checker;
    type_checker.accept(parse_tree);
    string file_name = argv[1];
    file_name.replace(file_name.end() - 3, file_name.end(), kAsm);
    std::ofstream target_ofstream(file_name);
    Compiler compiler;
    compiler.SetOuputStream(target_ofstream);
    compiler.SetIdentifiersMap(type_checker.GetIdentifierMap());
    compiler.accept(parse_tree);
    return 0;
  }
  return 1;
}

