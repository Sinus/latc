#include "variable.h"

using std::string;

namespace compiler {

Variable::Variable(const type& type) : type_(type) {}

type Variable::GetType() const {
  return type_;
}

string Variable::GetStackLocation() const {
  return stack_location_;
}

void Variable::SetStackLocation(string location) {
  stack_location_ = location;
}

}  // namespace compiler
