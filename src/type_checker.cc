#include "type_checker.h"

#include <iostream>
#include <vector>

#include "err.h"

using std::string;
using std::cout;
using std::endl;
using std::vector;

namespace compiler {
namespace {

constexpr char kPrintInt[] = "printInt";
constexpr char kPrintString[] = "printString";
constexpr char kPrintBool[] = "printBoolean";
constexpr char kReadInt[] = "readInt";
constexpr char kReadString[] = "readString";
constexpr char kError[] = "error";
const string kMain = "main";

}

void TypeChecker::visitProgram(Program* t) {} //abstract class
void TypeChecker::visitTopDef(TopDef* t) {} //abstract class
void TypeChecker::visitArg(Arg* t) {} //abstract class
void TypeChecker::visitBlock(Block* t) {} //abstract class
void TypeChecker::visitStmt(Stmt* t) {} //abstract class
void TypeChecker::visitItem(Item* t) {} //abstract class
void TypeChecker::visitType(Type* t) {} //abstract class
void TypeChecker::visitExpr(Expr* t) {} //abstract class
void TypeChecker::visitAddOp(AddOp* t) {} //abstract class
void TypeChecker::visitMulOp(MulOp* t) {} //abstract class
void TypeChecker::visitRelOp(RelOp* t) {} //abstract class

TriLogic TypeChecker::EvalsTo(Expr* expr) const {
  if (ELitTrue* _ = dynamic_cast<ELitTrue*>(expr)) {
    return TriLogic::TRUE;
  } else if (ELitFalse* _ = dynamic_cast<ELitFalse*>(expr)) {
    return TriLogic::FALSE;
  } else if (EOr* eor = dynamic_cast<EOr*>(expr)) {
    auto val1 = EvalsTo(eor->expr_1);
    auto val2 = EvalsTo(eor->expr_2);
    if (val1 == TriLogic::TRUE || val2 == TriLogic::TRUE)
      return TriLogic::TRUE;
    else if (val1 == TriLogic::UNKNOWN || val2 == TriLogic::UNKNOWN)
      return TriLogic::UNKNOWN;
    else
      return TriLogic::FALSE;
  } if (EAnd* eand = dynamic_cast<EAnd*>(expr)) {
    auto val1 = EvalsTo(eand->expr_1);
    auto val2 = EvalsTo(eand->expr_2);
    if (val1 == TriLogic::FALSE || val2 == TriLogic::FALSE)
      return TriLogic::FALSE;
    else if (val1 == TriLogic::UNKNOWN || val2 == TriLogic:: UNKNOWN)
      return TriLogic::UNKNOWN;
    else
      return TriLogic::TRUE;
  } else if (Not* enot = dynamic_cast<Not*>(expr)) {
    auto val = EvalsTo(enot->expr_);
    if (val == TriLogic::TRUE)
      return TriLogic::FALSE;
    else if (val == TriLogic::FALSE)
      return TriLogic::TRUE;
    else
      return TriLogic::UNKNOWN;
  } else {
    return TriLogic::UNKNOWN;
  }
}

void TypeChecker::accept(Visitable* v) {
  v->accept(this);
}

IdentifierMap* TypeChecker::GetIdentifierMap() {
  return &identifier_map_;
}

void TypeChecker::visitProg(Prog *prog)
{
  // Builtin functions.
  vector<type> print_int_arguments = {types::type_int};
  Function print_int(print_int_arguments, types::type_void);
  vector<type> print_bool_arguments = {types::type_bool};
  Function print_bool(print_bool_arguments, types::type_void);
  vector<type> print_string_arguments = {types::type_string};
  Function print_string(print_string_arguments, types::type_void);
  vector<type> read_int_arguments = {};
  Function read_int(read_int_arguments, types::type_int);
  vector<type> read_string_arguments = {};
  Function read_string(read_string_arguments, types::type_string);
  vector<type> error_arguments = {};
  Function error(error_arguments, types::type_void);

  identifier_map_.DeclareFunction(kPrintInt, print_int);
  identifier_map_.DeclareFunction(kPrintBool, print_bool);
  identifier_map_.DeclareFunction(kPrintString, print_string);
  identifier_map_.DeclareFunction(kReadInt, read_int);
  identifier_map_.DeclareFunction(kReadString, read_string);
  identifier_map_.DeclareFunction(kError, error);

  prog->listtopdef_->accept(this);

  if (!identifier_map_.IsFunction(kMain) ||
      identifier_map_.GetFunction(kMain).GetReturnType() != types::type_int ||
      identifier_map_.GetFunction(kMain).GetArgumentTypes().size() != 0)
    fatal("Wrong or no definition of main.", 0);
}

void TypeChecker::DeclareFunction(FnDef *fndef) {
  type return_type = types::TypeClassToType(fndef->type_);
  vector<type> argument_types;

  // identifiers_block_ is not used yet, we can use it here.

  for (int i = 0; i < fndef->listarg_->size(); ++i) {
    auto arg = fndef->listarg_->at(i);
    if (Ar* ar = dynamic_cast<Ar*>(arg)) {
      auto type = types::TypeClassToType(ar->type_);
      if (type == types::type_void)
        fatal("Void argument " + ar->ident_, fndef->line_number);
      argument_types.push_back(types::TypeClassToType(ar->type_));
      if (identifiers_block_.find(ar->ident_) != identifiers_block_.end())
        RedefinitionError(ar->ident_, fndef->line_number);
      identifiers_block_.insert(ar->ident_);
    }
  }

  if (identifier_map_.IsFunction(fndef->ident_))
    fatal("Redefinition of function " + fndef->ident_, fndef->line_number);

  Function function(argument_types, return_type);
  identifier_map_.DeclareFunction(fndef->ident_, function);

  identifiers_block_.clear();
}

void TypeChecker::visitFnDef(FnDef *fndef)
{
  identifiers_block_.clear();
  identifier_map_.ClearVariables();

  for (int i = 0; i < fndef->listarg_->size(); ++i) {
    if (Ar* ar = dynamic_cast<Ar*>(fndef->listarg_->at(i))) {
      // Declare all variables in current block.
      Variable argument(types::TypeClassToType(ar->type_));
      identifier_map_.DeclareVariable(ar->ident_, argument);
      identifiers_block_.insert(ar->ident_);
    }
    
  }
  returned_ = false;
  expected_return_type_ = types::TypeClassToType(fndef->type_);
  fndef->block_->accept(this);
  if (!returned_ && expected_return_type_ != types::type_void)
    fatal("Function " + fndef->ident_ + " does not return",
          fndef->line_number);
}

void TypeChecker::visitAr(Ar *ar)
{
  /* Code For Ar Goes Here */

  ar->type_->accept(this);
  visitIdent(ar->ident_);

}

void TypeChecker::visitBlk(Blk *blk)
{
  /* Code For Blk Goes Here */
  StoreEnvironment();
  blk->liststmt_->accept(this);
  RestoreEnvironment();
}

void TypeChecker::visitEmpty(Empty *empty)
{
  /* Code For Empty Goes Here */


}

void TypeChecker::visitBStmt(BStmt *bstmt)
{
  /* Code For BStmt Goes Here */

  bstmt->block_->accept(this);

}

void TypeChecker::visitDecl(Decl *decl)
{
  type var_type = types::TypeClassToType(decl->type_);
  if (var_type == types::type_void)
    fatal("Declaration of void variable", decl->line_number);
  for (const auto item : *decl->listitem_) {
    if (NoInit* noinit = dynamic_cast<NoInit*>(item)){
      if (identifiers_block_.find(noinit->ident_) != identifiers_block_.end()) {
        RedefinitionError(noinit->ident_, noinit->line_number);
      } else {
        Variable variable(var_type);
        identifiers_block_.insert(noinit->ident_);
        identifier_map_.DeclareVariable(noinit->ident_, variable);
      }
    } else if (Init* init = dynamic_cast<Init*>(item)) {
      if (identifiers_block_.find(init->ident_) != identifiers_block_.end()) {
        RedefinitionError(init->ident_, init->line_number);
      } else {
        auto expr_type = ExpressionType(init->expr_);
        if (expr_type != var_type)
          TypeMismatch(var_type, expr_type, init->line_number);
        Variable variable(var_type);
        identifiers_block_.insert(init->ident_);
        identifier_map_.DeclareVariable(init->ident_, variable);
      }
    }
  }
}

void TypeChecker::visitAss(Ass *ass)
{
  if (!identifier_map_.IsVariable(ass->ident_))
    UndeclaredVariable(ass->ident_, ass->line_number);
  type var_type = identifier_map_.GetVariable(ass->ident_).GetType();
  auto expr_type = ExpressionType(ass->expr_);
  if (expr_type != var_type)
    TypeMismatch(var_type, expr_type, ass->line_number);
}

void TypeChecker::visitIncr(Incr *incr)
{
  if (!identifier_map_.IsVariable(incr->ident_))
    UndeclaredVariable(incr->ident_, incr->line_number);
  auto var_type = identifier_map_.GetVariable(incr->ident_).GetType();
  if (var_type != types::type_int)
    TypeMismatch(types::type_int, var_type, incr->line_number);
}

void TypeChecker::visitDecr(Decr *decr)
{
  if (!identifier_map_.IsVariable(decr->ident_))
    UndeclaredVariable(decr->ident_, decr->line_number);
  auto var_type = identifier_map_.GetVariable(decr->ident_).GetType();
  if (var_type != types::type_int)
    TypeMismatch(types::type_int, var_type, decr->line_number);
}

void TypeChecker::visitRet(Ret *ret)
{
  returned_ = true;
  auto return_type = ExpressionType(ret->expr_);
  if (return_type != expected_return_type_)
    TypeMismatch(expected_return_type_, return_type, ret->line_number);
}

void TypeChecker::visitVRet(VRet *vret)
{
  returned_ = true;
  if (expected_return_type_ != types::type_void)
    TypeMismatch(expected_return_type_, types::type_void, vret->line_number);
}

template<class T>
void TypeChecker::CheckBool(T cond) const {
  auto cond_type = ExpressionType(cond->expr_);
  if (cond_type != types::type_bool)
    TypeMismatch(types::type_bool, cond_type, cond->line_number);
}

void TypeChecker::visitCond(Cond *cond)
{
  CheckBool(cond);
  auto old_returned = returned_;
  StoreEnvironment();
  cond->stmt_->accept(this);
  if (EvalsTo(cond->expr_) != TriLogic::TRUE)
    returned_ = old_returned;
  RestoreEnvironment();
}

void TypeChecker::visitCondElse(CondElse *condelse)
{
  auto old_returned = returned_;
  CheckBool(condelse);
  StoreEnvironment();
  condelse->stmt_1->accept(this);
  auto if_returned = returned_;
  returned_ = false;
  RestoreEnvironment();
  condelse->stmt_2->accept(this);
  auto else_returned = returned_;
  if (returned_ && if_returned)
    returned_ = true;
  else
    returned_ = old_returned;
  if (EvalsTo(condelse->expr_) == TriLogic::TRUE &&
      if_returned)
    returned_ = true;
  else if (EvalsTo(condelse->expr_) == TriLogic::FALSE &&
      else_returned)
    returned_ = true;
  RestoreEnvironment();

}

void TypeChecker::visitWhile(While *iwhile)
{
  CheckBool(iwhile);
  StoreEnvironment();
  iwhile->stmt_->accept(this);
  RestoreEnvironment();
}

void TypeChecker::visitSExp(SExp *sexp) {
  ExpressionType(sexp->expr_);
}

void TypeChecker::visitNoInit(NoInit *noinit)
{                               
  /* Code For NoInit Goes Here */

  visitIdent(noinit->ident_);

}

void TypeChecker::visitInit(Init *init)
{
  /* Code For Init Goes Here */

  visitIdent(init->ident_);
  init->expr_->accept(this);

}

void TypeChecker::visitInt(Int *eint)
{
  /* Code For Int Goes Here */


}

void TypeChecker::visitStr(Str *str)
{
  /* Code For Str Goes Here */


}

void TypeChecker::visitBool(Bool *ebool)
{
  /* Code For Bool Goes Here */


}

void TypeChecker::visitVoid(Void *evoid)
{
  /* Code For Void Goes Here */


}

void TypeChecker::visitFun(Fun *fun)
{
  /* Code For Fun Goes Here */

  fun->type_->accept(this);
  fun->listtype_->accept(this);

}

void TypeChecker::visitEVar(EVar *evar)
{
  /* Code For EVar Goes Here */

  visitIdent(evar->ident_);

}

void TypeChecker::visitELitInt(ELitInt *elitint)
{
  /* Code For ELitInt Goes Here */

  visitInteger(elitint->integer_);

}

void TypeChecker::visitELitTrue(ELitTrue *elittrue)
{
  /* Code For ELitTrue Goes Here */


}

void TypeChecker::visitELitFalse(ELitFalse *elitfalse)
{
  /* Code For ELitFalse Goes Here */


}

void TypeChecker::visitEApp(EApp *eapp)
{
  /* Code For EApp Goes Here */

  visitIdent(eapp->ident_);
  eapp->listexpr_->accept(this);

}

void TypeChecker::visitEString(EString *estring)
{
  /* Code For EString Goes Here */

  visitString(estring->string_);

}

void TypeChecker::visitNeg(Neg *neg)
{
  /* Code For Neg Goes Here */

  neg->expr_->accept(this);

}

void TypeChecker::visitNot(Not *enot)
{
  /* Code For Not Goes Here */

  enot->expr_->accept(this);

}

void TypeChecker::visitEMul(EMul *emul)
{
  /* Code For EMul Goes Here */

  emul->expr_1->accept(this);
  emul->mulop_->accept(this);
  emul->expr_2->accept(this);

}

void TypeChecker::visitEAdd(EAdd *eadd)
{
  /* Code For EAdd Goes Here */

  eadd->expr_1->accept(this);
  eadd->addop_->accept(this);
  eadd->expr_2->accept(this);

}

void TypeChecker::visitERel(ERel *erel)
{
  /* Code For ERel Goes Here */

  erel->expr_1->accept(this);
  erel->relop_->accept(this);
  erel->expr_2->accept(this);

}

void TypeChecker::visitEAnd(EAnd *eand)
{
  /* Code For EAnd Goes Here */

  eand->expr_1->accept(this);
  eand->expr_2->accept(this);

}

void TypeChecker::visitEOr(EOr *eor)
{
  /* Code For EOr Goes Here */

  eor->expr_1->accept(this);
  eor->expr_2->accept(this);

}

void TypeChecker::visitPlus(Plus *plus)
{
  /* Code For Plus Goes Here */


}

void TypeChecker::visitMinus(Minus *minus)
{
  /* Code For Minus Goes Here */


}

void TypeChecker::visitTimes(Times *times)
{
  /* Code For Times Goes Here */


}

void TypeChecker::visitDiv(Div *div)
{
  /* Code For Div Goes Here */


}

void TypeChecker::visitMod(Mod *mod)
{
  /* Code For Mod Goes Here */


}

void TypeChecker::visitLTH(LTH *lth)
{
  /* Code For LTH Goes Here */


}

void TypeChecker::visitLE(LE *le)
{
  /* Code For LE Goes Here */


}

void TypeChecker::visitGTH(GTH *gth)
{
  /* Code For GTH Goes Here */


}

void TypeChecker::visitGE(GE *ge)
{
  /* Code For GE Goes Here */


}

void TypeChecker::visitEQU(EQU *equ)
{
  /* Code For EQU Goes Here */


}

void TypeChecker::visitNE(NE *ne)
{
  /* Code For NE Goes Here */


}


void TypeChecker::visitListTopDef(ListTopDef* listtopdef)
{
  for (ListTopDef::iterator i = listtopdef->begin() ; i != listtopdef->end() ; ++i)
  {
    if (FnDef* fndef = dynamic_cast<FnDef*>(*i))
      DeclareFunction(fndef);
  }
  for (ListTopDef::iterator i = listtopdef->begin() ; i != listtopdef->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitListArg(ListArg* listarg)
{
  for (ListArg::iterator i = listarg->begin() ; i != listarg->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitListStmt(ListStmt* liststmt)
{
  for (ListStmt::iterator i = liststmt->begin() ; i != liststmt->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitListItem(ListItem* listitem)
{
  for (ListItem::iterator i = listitem->begin() ; i != listitem->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitListType(ListType* listtype)
{
  for (ListType::iterator i = listtype->begin() ; i != listtype->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitListExpr(ListExpr* listexpr)
{
  for (ListExpr::iterator i = listexpr->begin() ; i != listexpr->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void TypeChecker::visitInteger(Integer x)
{
  /* Code for Integer Goes Here */
}

void TypeChecker::visitChar(Char x)
{
  /* Code for Char Goes Here */
}

void TypeChecker::visitDouble(Double x)
{
  /* Code for Double Goes Here */
}

void TypeChecker::visitString(String x)
{
  /* Code for String Goes Here */
}

void TypeChecker::visitIdent(Ident x)
{
  /* Code for Ident Goes Here */
}

template<class T>
void TypeChecker::RecursiveExpressionCheck(T* expr, type expected) const {
  type expr1_type = ExpressionType(expr->expr_1);
  type expr2_type = ExpressionType(expr->expr_2);
  if (expr1_type != expected)
    TypeMismatch(expected, expr1_type, expr->line_number);
  if (expr2_type != expected)
    TypeMismatch(expected, expr2_type, expr->line_number);
}

type TypeChecker::CheckAdd(EAdd* expr) const {
  auto expr1_type = ExpressionType(expr->expr_1);
  auto expr2_type = ExpressionType(expr->expr_2);
  if (expr1_type != expr2_type)
    fatal("Addition of " + expr1_type + " and " + expr2_type,
        expr->line_number);
  if (expr1_type != types::type_int && expr1_type != types::type_string)
    fatal("Addition of " + expr1_type, expr->line_number);
  return expr1_type;
}

type TypeChecker::CheckEqual(ERel* expr) const {
  auto expr1_type = ExpressionType(expr->expr_1);
  auto expr2_type = ExpressionType(expr->expr_2);
  if (expr1_type != expr2_type)
    fatal("Comparison of " + expr1_type + " and " + expr2_type,
        expr->line_number);
  return expr1_type;
}

type TypeChecker::ExpressionType(Expr* expr) const {
  if (EVar* evar = dynamic_cast<EVar*>(expr)) {
    if (identifier_map_.IsVariable(evar->ident_))
      return identifier_map_.GetVariable(evar->ident_).GetType();
    else
      UndeclaredVariable(evar->ident_, evar->line_number); 
  } else if (ELitInt* elitint = dynamic_cast<ELitInt*>(expr)) {
    return types::type_int;
  } else if (ELitTrue* elittrue = dynamic_cast<ELitTrue*>(expr)) {
    return types::type_bool;
  } else if (ELitFalse* elitfalse = dynamic_cast<ELitFalse*>(expr)) {
    return types::type_bool;
  } else if (EString* estring = dynamic_cast<EString*>(expr)) {
    return types::type_string;
  } else if (EOr* bexp = dynamic_cast<EOr*>(expr)) {
    RecursiveExpressionCheck(bexp, types::type_bool);
    return types::type_bool;
  } else if (EAnd* bexp = dynamic_cast<EAnd*>(expr)) {
    RecursiveExpressionCheck(bexp, types::type_bool);
    return types::type_bool;
  } else if (Not* neg = dynamic_cast<Not*>(expr)) {
    type negated_type = ExpressionType(neg->expr_);
    if (negated_type != types::type_bool)
      TypeMismatch(types::type_bool, negated_type, neg->line_number);
    return types::type_bool;
  } else if (Neg* neg = dynamic_cast<Neg*>(expr)) {
    type negated_type = ExpressionType(neg->expr_);
    if (negated_type != types::type_int)
      TypeMismatch(types::type_int, negated_type, neg->line_number);
    return types::type_int;
  } else if (EAdd* eadd = dynamic_cast<EAdd*>(expr)) {
    auto rel_operator = eadd->addop_;
    if (Plus* plus = dynamic_cast<Plus*>(rel_operator))
      return CheckAdd(eadd);
    RecursiveExpressionCheck(eadd, types::type_int);
    return types::type_int;
  } else if (EMul* emul = dynamic_cast<EMul*>(expr)) {
    RecursiveExpressionCheck(emul, types::type_int);
    return types::type_int;
  } else if (ERel* erel = dynamic_cast<ERel*>(expr)) {
    auto rel_operator = erel->relop_; 
    if (EQU* equ = dynamic_cast<EQU*>(rel_operator)) {
      CheckEqual(erel);
      return types::type_bool;
    } else if (NE* ne = dynamic_cast<NE*>(rel_operator)) {
      CheckEqual(erel);
      return types::type_bool;
    } else {
      RecursiveExpressionCheck(erel, types::type_int);
      return types::type_bool;
    }
  } 
  else if (EApp* eapp = dynamic_cast<EApp*>(expr)) {
    if (!identifier_map_.IsFunction(eapp->ident_))
      UndeclaredVariable(eapp->ident_, eapp->line_number);
    auto function = identifier_map_.GetFunction(eapp->ident_);
    auto function_arguments = function.GetArgumentTypes();
    
    if (function_arguments.size() != eapp->listexpr_->size())
      WrongNumberOfArguments(function_arguments.size(), eapp->listexpr_->size(),
                             eapp->ident_, eapp->line_number);
    
    for (int i = 0; i < function_arguments.size(); ++i) {
      auto received_type = ExpressionType(eapp->listexpr_->at(i));
      auto expected_type = function_arguments.at(i);
      if (expected_type != received_type)
        TypeMismatch(expected_type, received_type, eapp->line_number);
    }
    return function.GetReturnType();
  } else {
    return types::type_any;  // This should never be reached.
  }
}

void TypeChecker::StoreEnvironment() {
  identifier_map_copy = identifier_map_.GetVariables();
  identifiers_block_copy_ = identifiers_block_;
  identifiers_block_.clear();
}

void TypeChecker::RestoreEnvironment() {
  identifiers_block_ = identifiers_block_copy_;
  identifier_map_.RestoreVariables(identifier_map_copy);
}

}  // namespace compiler

