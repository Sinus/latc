#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>

#define SIZE (1024)

static uint64_t references[SIZE][2];
static uint64_t min_malloc_addr = 0 - 1;

int64_t readInt() {
  int64_t x;
  scanf("%" PRId64 "", &x);
  char c;

  // Flush the stdin
  while ((c = getchar()) != '\n' && c != EOF) {}
  
  return x;
}

char* readString() {
  char* line = NULL;
  size_t size;
  if (getline(&line, &size, stdin) == -1)
    fprintf(stderr, "Runtime error in readString(): %s\n", strerror(errno));

  line[strlen(line) - 1] = '\0';  // Remove trailing newline.
  
  if ((uint64_t) line < min_malloc_addr)
    min_malloc_addr = (uint64_t) line;
  return line;
}

void printInt(int64_t x) {
  printf("%" PRId64 "\n", x);
}

void printString(char* x) {
  printf("%s\n", x);
}

char* add_strings(char* a, char* b) {
  char* buffer = (char* ) malloc((strlen(a) + strlen(b)) * sizeof(char) + sizeof(char));
  if (buffer == NULL)
    exit(EXIT_FAILURE);
  memset(buffer, 0, sizeof(buffer)/sizeof(buffer[0]));
  strcpy(buffer, a);
  strcat(buffer, b);

  if ((uint64_t) buffer < min_malloc_addr)
    min_malloc_addr = (uint64_t) buffer;

  return buffer;
}

static int find(uint64_t addr) {
  for (int i = 0; i < SIZE; i++) {
  if (references[i][0] == addr)
    return i;
  }
  return 0;
}

static int get_free() {
  for (int i = 0; i < SIZE; i++)
    if (references[i][0] == 0)
      return i;
  return 0;
}

void gc_init() {
  for (int i = 0; i < SIZE; i++) {
    references[i][0] = 0;
    references[i][1] = 0;
  }
}

void add_reference(uint64_t addr) {
  if (references[find(addr)][0] == addr)
    references[find(addr)][1]++;
  else {
    int index = get_free();
    references[index][0] = addr;
    references[index][1] = 1;
  }
}

void unlink(uint64_t addr) {
  if (references[find(addr)][0] == addr) {
    references[find(addr)][1]--;
    if (references[find(addr)][1] == 0) {
      char* actual_addr = (char*)addr;
      if (addr >= min_malloc_addr)
        free(actual_addr);
      references[find(addr)][0] = 0;
    }
  }
}
