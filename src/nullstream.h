#ifndef NULLSTREAM_H_
#define NULLSTREAM_H_

#include <iostream>

class NullStream : public std::ostream {
 public:
  int overflow(int c) { return c; }
};

#endif
