#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <string>
#include <vector>

#include "type.h"

namespace compiler {

// This class represents a function defined by programer.
// It stores types of arguments, return type and other properties.
class Function {
 public:
  // Default empty constructor.
  Function() = default;

  // Default empty destructor.
  ~Function() = default;

  // Constructs Function of given argument types and return type.
  Function(const std::vector<type>& argument_types,
      const type& return_type);

  // Returns return type of this Function.
  type GetReturnType() const;
  
  // Return a vector of types of arguments of this Function.
  // If this Function takes no arguments, the returned vector is empty.
  std::vector<type> GetArgumentTypes() const;

 private:
  type return_type_;
  std::vector<type> argument_types_;
};

}  // namespace compiler

#endif
