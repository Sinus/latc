#ifndef ERR_H_
#define ERR_H_
#include <string>

#include "type.h"

namespace compiler {

// Log message to stderr and exit the program with error code -1.
void fatal(const std::string& message, int line_number);

// Logs usage of undeclared variable.
// Exits with error code -1.
void UndeclaredVariable(const std::string& ident, int line_number);

// Logs messege to stderr informing about type mismatch.
// Exits with error code -1.
void TypeMismatch(const type& expected, const type& received, int line_number);

// Log mismatch of expected and received return type.
// Exits with error code -1.
void ReturnTypeMismatch(const type& expected, const type& received);

// Log returning value from void function.
// Exits with error code -1.
void ReturnFromVoid();

// Log passing wrong number of types to function call.
// Exits with error code -1.
void WrongNumberOfArguments(int expected, int received,
                            const std::string& ident, int line_number);

// Log redefinition of given identifier in one scope.
// Exists with error code -1.
void RedefinitionError(const std::string& name, int line_number);

}  // namespace compiler
#endif
