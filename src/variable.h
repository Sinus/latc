#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <string>

#include "type.h"

namespace compiler {

// This class represents a variable used by the programmer.
// It stores type, location and other properties.
class Variable {
 public:
  // Default empty constructor.
  Variable() = default;

  // Default empty destructor.
  ~Variable() = default;

  // Given a type creates a new Variable.
  explicit Variable(const type& type);

  // Returns type of this variable.
  std::string GetType() const;

  // Returns location of the variable on stack.
  std::string GetStackLocation() const;

  // Sets location of variable on stack.
  void SetStackLocation(std::string);

 private:
  // Type of the variable.
  type type_;

  // Location of the variable on stack.
  std::string stack_location_;
};

}  // namespace compiler

#endif
