#ifndef COMPILER_H_
#define COMPILER_H_

#include <ostream>
#include <string>
#include <vector>

#include "../lib/Absyn.H"
#include "identifier_map.h"
#include "tri_logic.h"
#include "type_checker.h"

namespace compiler {

class Compiler : public Visitor
{
 public:
  void accept(Visitable* v);
  void SetOuputStream(std::ostream& ostream);
  void visitProgram(Program* p);
  void visitTopDef(TopDef* p);
  void visitArg(Arg* p);
  void visitBlock(Block* p);
  void visitStmt(Stmt* p);
  void visitItem(Item* p);
  void visitType(Type* p);
  void visitExpr(Expr* p);
  void visitAddOp(AddOp* p);
  void visitMulOp(MulOp* p);
  void visitRelOp(RelOp* p);
  void visitProg(Prog* p);
  void visitFnDef(FnDef* p);
  void visitAr(Ar* p);
  void visitBlk(Blk* p);
  void visitEmpty(Empty* p);
  void visitBStmt(BStmt* p);
  void visitDecl(Decl* p);
  void visitNoInit(NoInit* p);
  void visitInit(Init* p);
  void visitAss(Ass* p);
  void visitIncr(Incr* p);
  void visitDecr(Decr* p);
  void visitRet(Ret* p);
  void visitVRet(VRet* p);
  void visitCond(Cond* p);
  void visitCondElse(CondElse* p);
  void visitWhile(While* p);
  void visitSExp(SExp* p);
  void visitInt(Int* p);
  void visitStr(Str* p);
  void visitBool(Bool* p);
  void visitVoid(Void* p);
  void visitFun(Fun* p);
  void visitEVar(EVar* p);
  void visitELitInt(ELitInt* p);
  void visitELitTrue(ELitTrue* p);
  void visitELitFalse(ELitFalse* p);
  void visitEApp(EApp* p);
  void visitEString(EString* p);
  void visitNeg(Neg* p);
  void visitNot(Not* p);
  void visitEMul(EMul* p);
  void visitEAdd(EAdd* p);
  void visitERel(ERel* p);
  void visitEAnd(EAnd* p);
  void visitEOr(EOr* p);
  void visitPlus(Plus* p);
  void visitMinus(Minus* p);
  void visitTimes(Times* p);
  void visitDiv(Div* p);
  void visitMod(Mod* p);
  void visitLTH(LTH* p);
  void visitLE(LE* p);
  void visitGTH(GTH* p);
  void visitGE(GE* p);
  void visitEQU(EQU* p);
  void visitNE(NE* p);
  void visitListTopDef(ListTopDef* p);
  void visitListArg(ListArg* p);
  void visitListStmt(ListStmt* p);
  void visitListItem(ListItem* p);
  void visitListType(ListType* p);
  void visitListExpr(ListExpr* p);

  void visitInteger(Integer x);
  void visitChar(Char x);
  void visitDouble(Double x);
  void visitString(String x);
  void visitIdent(Ident x);

  void SetIdentifiersMap(IdentifierMap* identifier_map);

 private:
  // Rerutns a stream to which bytecode can be appended.
  std::ostream& Bytecode_() const;

  // Generate bytecode for exit(return_code).
  std::string SysExit_(int return_code) const;

  // Generate bytecode for lea reg, [item]
  std::string Lea_(std::string reg, std::string item) const;

  // Generate bytecode for mov instruction of item to register.
  std::string Mov_(std::string reg, std::string item) const;

  // Generate bytecode for sub instruction from given register.
  std::string Sub_(std::string reg, std::string item) const;

  // Given a string return [string].
  std::string StringConstantLocation_(int location_number) const;

  // Return length of n-th constant string.
  std::string StringConstantLength_(int location_number) const;

  // Generate bytecode for sys_write for constant strings.
  std::string WriteConstantToFd_(int constant, int fd) const;

  // Stores identifiers map.
  void StoreEnvironment();

  // Restores identifiers map from StoreEnvironment.
  // 
  // NOTE: Behaviour is undefined if RestoreEnvironment is called when
  // no environment is saved prior to that call.
  void RestoreEnvironment();

  // Assign value from rax to given variable.
  void Assign(const Variable& variable) const;

  // Move value of expression adn put result in RAX.
  // is_string should be set to true if expresseion is calculated for strings.
  void CalculateExpression(Expr* expr, bool is_string = false);

  // Returns first free label index.
  int GetLabel();

  // Copy of identifier_map_, used by Store/Restore Environment.
  std::unordered_map<std::string, Variable> identifier_map_copy_;

  // Is true if body of function which is currently compiled returns a string.
  bool current_function_returns_string_;

  // True if we are in a while loop.
  bool in_while_ = false;

  // How much should the rsp be modified in a loop.
  int rsp_modif_ = 0;

  // Index of first free label.
  int free_label_ = 0;

  // Stream to which bytecode will be written.
  mutable std::ostream* stream_;

  // Mapping of variables, stores information about locations etc.
  IdentifierMap* identifier_map_;

  TypeChecker type_checker_;

  // A set/list of string literals used in the program.
  std::vector<std::string> constant_strings_; 

  // Offset from rbp pointing to space avaiable for new variable.
  int current_rbp_offset_ = 0;

};

}  // namespace compiler

#endif
