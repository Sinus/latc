#ifndef TYPE_H_
#define TYPE_H_

#include <string>

#include "../lib/Absyn.H"

namespace compiler {

typedef std::string type;

namespace types {

const type type_int = "int";
const type type_string = "string";
const type type_void = "void";
const type type_bool = "boolean";
const type type_any = "any";

static type TypeClassToType(Type* type) {
  if (Int* vint = dynamic_cast<Int*>(type))
    return types::type_int;
  if (Str* vstr = dynamic_cast<Str*>(type))
    return types::type_string;
  if (Void* vvoid = dynamic_cast<Void*>(type))
    return types::type_void;
  if (Bool* vbool = dynamic_cast<Bool*>(type))
    return types::type_bool;
  return "unknown";  // This should never happen.
}

}  // namespace types
}  // namespace compiler
#endif
