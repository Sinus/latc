#include <iostream>
#include <string>
#include <cstdlib>
#include "err.h"

using std::cerr;
using std::endl;
using std::string;

namespace compiler {

constexpr char kError[] = "ERROR";
constexpr char kReturnMismatch[] = "Return type mismatch: ";
constexpr char kTypeMismatch[] = "Type mismatch: ";
constexpr char kTypeExpected[] = "expected type ";
constexpr char kTypeReceived[] = " but received ";
constexpr char kReturnFromVoid[] = "Returning value from void function.";
constexpr char kRedefinition[] = "Redefinition of indentifier: ";
constexpr char kWrongArgumentCount[] = "Wrong number of arguments passed to function call ";
constexpr char kCountExpected[] = " expected ";
constexpr char kCountReceived[] = ", but received ";
constexpr char kUndeclaredVariable[] = "Undeclared identfier ";
constexpr char kInLine[] = " in line ";
constexpr char kEnd[] = ".\n";

void fatal(const std::string& message, int line_number) {
  cerr << kError << endl;
  cerr << message << kInLine << line_number << kEnd;
  exit(EXIT_FAILURE);
}

void TypeMismatch(const type& expected, const type& received,
                  int line_number) {
  cerr << kError << endl;
  cerr << kTypeMismatch << kTypeExpected << expected
    << kTypeReceived << received << kInLine << line_number << kEnd;
  exit(EXIT_FAILURE);
}

void ReturnTypeMismatch(const type& expected, const type& received) {
  cerr << kError << endl;
  cerr << kReturnMismatch << kTypeExpected << expected
    << kTypeReceived << received << "." << endl;
  exit(EXIT_FAILURE);
}

void ReturnFromVoid() {
  cerr << kError << endl;
  cerr << kReturnFromVoid << endl;
  exit(EXIT_FAILURE);
}

void RedefinitionError(const string& name, int line_number) {
  cerr << kError << endl;
  cerr << kRedefinition << name << kInLine << line_number << kEnd;
  exit(EXIT_FAILURE);
}

void WrongNumberOfArguments(int expected, int received, const string& ident,
                            int line_number) {
  cerr << kError << endl;
  cerr << kWrongArgumentCount << ident << kCountExpected << expected 
    << kCountReceived << received << kInLine << line_number << kEnd;
  exit(EXIT_FAILURE);
}

void UndeclaredVariable(const std::string& ident, int line_number) {
  cerr << kError << endl;
  cerr << kUndeclaredVariable << ident << kInLine << kEnd;
  exit(EXIT_FAILURE);
}

}  // namespace compiler
